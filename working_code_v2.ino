/**************************************************************************/
/*! 
    inspired from   Adafruit Industries exemple
*/
/**************************************************************************/
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_PN532.h>

// If using the breakout with SPI, define the pins for SPI communication.
#define PN532_SCK  (2)
#define PN532_MOSI (3)
#define PN532_SS   (4)
#define PN532_MISO (5)

// If using the breakout or shield with I2C, define just the pins connected
// to the IRQ and reset lines.  Use the values below (2, 3) for the shield!
#define PN532_IRQ   (2)
#define PN532_RESET (3)  // Not connected by default on the NFC Shield

// Uncomment just _one_ line below depending on how your breakout or shield
// is connected to the Arduino:

// Use this line for a breakout with a software SPI connection (recommended):
Adafruit_PN532 nfc(PN532_SCK, PN532_MISO, PN532_MOSI, PN532_SS);

// Use this line for a breakout with a hardware SPI connection.  Note that
// the PN532 SCK, MOSI, and MISO pins need to be connected to the Arduino's
// hardware SPI SCK, MOSI, and MISO pins.  On an Arduino Uno these are
// SCK = 13, MOSI = 11, MISO = 12.  The SS line can be any digital IO pin.
//Adafruit_PN532 nfc(PN532_SS);

// Or use this line for a breakout or shield with an I2C connection:
//Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);

#if defined(ARDUINO_ARCH_SAMD)
// for Zero, output on USB Serial console, remove line below if using programming port to program the Zero!
// also change #define in Adafruit_PN532.cpp library file
   #define Serial SerialUSB
#endif

void setup(void) {
  
  #ifndef ESP8266
    while (!Serial); // for Leonardo/Micro/Zero
  #endif
  Serial.begin(9600);
  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    while (1); // halt
  }

  // configure board to read RFID tags
  nfc.SAMConfig();
  
}


void loop(void) {
  uint8_t success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
    
  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
  
  if (success) {
    // Display some basic information about the card
    //Serial.println("Found an ISO14443A card");
    //Serial.print("  UID Length: ");Serial.print(uidLength, DEC);Serial.println(" bytes");
    //Serial.print("  UID Value: ");
    //nfc.PrintHex(uid, uidLength);
    //Serial.println("");
    
    if (uidLength == 4)
    {
      // We probably have a Mifare Classic card ... 
	  
      // Now we need to try to authenticate it for read/write access
      // Try with the factory default KeyA first: 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF
      //uint8_t keya[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }; //A0A1A2A3A4A5
      //uint8_t keya[6] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
      uint8_t keya[6] = { 0xD3, 0xF7, 0xD3, 0xF7, 0xD3, 0xF7 };
	  
	  // Start with block 4 (the first block of sector 1) since sector 0
	  // contains the manufacturer data and it's probably better just
	  // to leave it alone unless you know what you're doing
      success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, 7, 0, keya);
	  
      if (success)
      {        

        // Id = 24 caracteres donc on met le reste des caracteres dans le bloc suivant.
        uint8_t id1[16];
        uint8_t id2[16];
        uint8_t nom[16];

        // Write the id1 in block 4
        //memcpy(id1, (const uint8_t[]){ '5', 'c', '4', '3', 'e', '9', '4', 'f', '3', 'f', 'b', '1', '1', '0', '2', '7' }, sizeof id1);
        //success = nfc.mifareclassic_WriteDataBlock (4, id1);

        // Try to read the contents of block 4
        success = nfc.mifareclassic_ReadDataBlock(4, id1);


        // Write the id2 in block 5
        //memcpy(id2, (const uint8_t[]){ '3', '0', '8', '4', 'b', 'b', 'a', 'd', 0, 0, 0, 0, 0, 0, 0, 0 }, sizeof id2);
        //success = nfc.mifareclassic_WriteDataBlock (5, id2);

        // Try to read the contents of block 5
        success = nfc.mifareclassic_ReadDataBlock(5, id2);


        // Write nom in block 6
        //memcpy(nom, (const uint8_t[]){ 'G', 'U', 'E', 'T', 'A', 'N', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, sizeof nom);
        //success = nfc.mifareclassic_WriteDataBlock (6, nom);

        // Try to read the contents of block 6
        success = nfc.mifareclassic_ReadDataBlock(6, nom);
		
        if (success)
        { 
          // Data seems to have been read ...
          nfc.PrintHexChar(id1, 16);

          nfc.PrintHexChar(id2, 16);

          nfc.PrintHexChar(nom, 16);
      
          // Wait a bit before reading the card again
          delay(2000);
        }
        else
        {          
          Serial.println("error");
          //Serial.println("Ooops ... unable to read the requested block.  Try another key?");
        }
      }
      else
      {        
        Serial.println("error");
        //Serial.println("Ooops ... authentication failed: Try another key?");
      }
    }
    //if the uid is 7 bytes (Mifare Ultralight)
    else if (uidLength == 7) {
      //Not implemented yet
      Serial.println("error");
      delay(2000);
    }
  }
}
